# GMF QA Automation Bot #

This bot is used for automated testing for the GMF Project.

### How do I get set up? ###

To be able to use this bot, first locate the master branch and procced to change the credentials on credentials.json file then just run App.py file

### Set up Mail ###

It is important to set up filters in your email so you can recieve mails in a specific inbox to be able to do this please follow the these steps provided by google:

https://support.google.com/mail/answer/6579?hl=en

To use some functions of the bot you must have setted up the following categories in your email so the bot can read them


* *OTP*
* *TRX_OTP*
* *OTP_PWD_CHANGE*

### Contribution guidelines ###

Any contributions is welcome

### Who do I talk to? ###

* Andres Fanjul